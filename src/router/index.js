import { createRouter, createWebHistory } from 'vue-router'
import Player from '../views/Player.vue'

const routes = [
	{
		path: '/',
		redirect: '/view/r2'
	},
	{
		path: '/view/:id',
		name: 'Player',
		component: Player
	}
]

const router = createRouter({
	history: createWebHistory(process.env.BASE_URL),
	routes
})

export default router
