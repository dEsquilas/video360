# 360 Vue Player

## Project setup
```
npm install
```

Copy equirectangular video files to public/videos/ folder

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```


